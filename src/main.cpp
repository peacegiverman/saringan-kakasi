/*********************************************
 * Saringan ratnik kakasi sensej             *
 *                                           *
 * Author: Damir Veapi <cevapi.bg@gmail.com> *
 * *******************************************/
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"
#include "SDL/SDL_net.h"

#include "SDL/sge.h"

#include <string>
#include <cstdlib>
#include <cmath>
#include <vector>

#ifdef _WIN32
#define FONT_PATH "comic.ttf"
#else
#define FONT_PATH "/usr/share/fonts/truetype/msttcorefonts/comic.ttf"
#endif

//TODO: read from config
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int SCREEN_BPP = 32;
const int FPS = 30;

bool netplay = false;

int score = 0;

SDL_Surface* screen = NULL;
SDL_Surface* menubg = NULL;

SDL_Surface* txtScore = NULL;
SDL_Surface* txtOver = NULL;
SDL_Surface* txtSinglePlayer= NULL;
SDL_Surface* txtMultiPlayer= NULL;
SDL_Surface* txtServeGame = NULL;
SDL_Surface* txtJoinGame = NULL;

Uint32 bgcolor;

SDL_Event event;
Uint8 *keystate = NULL;

TTF_Font *font = NULL;
TTF_Font *menuFont = NULL;
SDL_Color textColor = {0,0,0};
SDL_Color selectedColor = {255,0,0};

Mix_Music *music = NULL;
Mix_Chunk *enemySnd = NULL;

TCPsocket sock, serverSock;
IPaddress ip, *remoteIP;
SDLNet_SocketSet sockSet;

SDL_Surface *load_image(std::string filename)
{
	SDL_Surface* loadedImage = NULL;
	SDL_Surface* optimizedImage = NULL;

	loadedImage = IMG_Load(filename.c_str());

	if (loadedImage!=NULL) {
		optimizedImage = SDL_DisplayFormatAlpha(loadedImage);
		SDL_FreeSurface(loadedImage);
	} else {
		fprintf(stderr, "IMG_Load: %s\n", IMG_GetError());
	}

	if (optimizedImage != NULL) {
		/*//Map the color key
		Uint32 colorkey = SDL_MapRGB(optimizedImage->format, 0, 0, 0xFF);

		//Make it transparent
		SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, colorkey);
		*/

		return optimizedImage;
	}
	return NULL;
}

void draw_surface(int x, int y, SDL_Surface* src, SDL_Surface* dst)
{
	//Make a temp rectangle to hold offsets
	SDL_Rect offset;

	offset.x = x;
	offset.y = y;

	//Blit the surface
	SDL_BlitSurface(src, NULL, dst, &offset);
}

bool init_sdl()
{
	//Start SDL
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "\nUnable to initialize SDL:  %s\n", SDL_GetError());
		return false;
	}
	
	srand(SDL_GetTicks());

	//Init screen to 800x600x32 with HW accell
	screen = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP,
				SDL_HWSURFACE);
	if (screen==NULL) {
		fprintf(stderr,"Could not set video mode: %s\n", SDL_GetError());
		return false;
	}
	
	bgcolor = SDL_MapRGB(screen->format, 255, 255, 255);
	SDL_FillRect(screen, NULL, bgcolor);

	//Init SDL_ttf
	if (TTF_Init() != 0) {
		fprintf(stderr, "Could not init SDL_ttf: %s\n", TTF_GetError());
		return 1;
	}

	//Init SDL_mixer
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) != 0) {
		fprintf(stderr, "Could not open mixer: %s\n", Mix_GetError());
		return 1;
	}
	
	//Set window caption
	SDL_WM_SetCaption("SARINGAN RATNIK KAKASI SENSEJ", NULL);

	return true;
}

bool load_data()
{
	//Open the font
	font = TTF_OpenFont(FONT_PATH, 32);
	if (font == NULL) {
		fprintf(stderr, "Could not load font: %s\n", TTF_GetError());
		return false;
	}

	menuFont = TTF_OpenFont(FONT_PATH, 24);
	if (menuFont == NULL) {
		fprintf(stderr, "Could not load font: %s\n", TTF_GetError());
		return false;
	}

	//Load music
	music = Mix_LoadMUS("mus/kakasi.mp3");
	if (music == NULL) {
		fprintf(stderr, "Could not load music: %s\n", Mix_GetError());
		return false;
	}

	enemySnd = Mix_LoadWAV("snd/ninja.wav");
	if(enemySnd == NULL) {
		fprintf(stderr, "Could not load sound effect: %s\n", Mix_GetError());
		return false;
	}

	menubg = load_image("img/menubg.png");

	txtSinglePlayer = TTF_RenderText_Solid(menuFont, "Single Player", textColor);
	txtMultiPlayer= TTF_RenderText_Solid(menuFont, "Co-op Game", textColor);

	txtServeGame = TTF_RenderText_Solid(menuFont, "Create Game", textColor);
	txtJoinGame = TTF_RenderText_Solid(menuFont, "Join Game", textColor);
	
	return true;
}

bool init_network()
{
	if(SDLNet_Init() != 0) {
		fprintf(stderr, "Could not init network: %s\n", SDLNet_GetError());
		return false;
	}

	return true;
}

bool serve_game()
{
	if(SDLNet_ResolveHost(&ip, NULL, 13200) != 0) {
		fprintf(stderr, "Could not start server: %s\n", SDLNet_GetError());
		return false;
	}

	serverSock = SDLNet_TCP_Open(&ip);
	if(!serverSock) {
		fprintf(stderr, "Could not open socket: %s\n", SDLNet_GetError());
		return false;
	}

	while(true) {
		sock = SDLNet_TCP_Accept(serverSock);
		if(sock) break;
	}

	sockSet = SDLNet_AllocSocketSet(1);
	if(!sockSet) {
		fprintf(stderr, "Could not allocate sock set: %s\n", SDLNet_GetError());
		return false;
	}

	if(!SDLNet_TCP_AddSocket(sockSet, sock)) {
		fprintf(stderr, "Error creating socket set: %s\n", SDLNet_GetError());
		return false;
	}

	return true;
}

bool join_game(char* serverAddress)
{
	if(SDLNet_ResolveHost(&ip, serverAddress, 13200) != 0) {
		fprintf(stderr, "Could not resolve host: %s\n", SDLNet_GetError());
		return false;
	}

	sock = SDLNet_TCP_Open(&ip);
	if(!sock) {
		fprintf(stderr, "Could not open socket: %s\n", SDLNet_GetError());
		return false;
	}
	
	sockSet = SDLNet_AllocSocketSet(1);
	if(!sockSet) {
		fprintf(stderr, "Could not allocate sock set: %s\n", SDLNet_GetError());
		return false;
	}

	if(!SDLNet_TCP_AddSocket(sockSet, sock)) {
		fprintf(stderr, "Error creating socket set: %s\n", SDLNet_GetError());
		return false;
	}

	return true;
}	


void cleanup()
{
	if(netplay)
		SDLNet_FreeSocketSet(sockSet);
	
	SDL_FreeSurface(txtScore);
	SDL_FreeSurface(txtOver);
		
	TTF_CloseFont(font);

	Mix_FreeMusic(music);
	Mix_FreeChunk(enemySnd);

	Mix_CloseAudio();
	TTF_Quit();
	//Quit SDL
	SDL_Quit();

	return;
}

template <class T, class K>
bool detectCollision(T t, K k)
{
	if(t->collisionBox.x+t->collisionBox.w >= k->collisionBox.x &&
	   t->collisionBox.x <= k->collisionBox.x + k->collisionBox.w &&
	   t->collisionBox.y+t->collisionBox.h >= k->collisionBox.y &&
	   t->collisionBox.y <= k->collisionBox.y + k->collisionBox.h)
	
		return true;
	else
		return false;
}

struct KunaiStruct
{
	int x,y;
	int xVel, yVel;
};


//Kunais that are thrown at enemies and HURT THEM A LOT!
class Kunai {
	private:


	//sprite
	SDL_Surface* sprite;

	public:
	//position and size
	int x,y;
	int w,h;

	int xVel, yVel;
	SDL_Rect collisionBox;

	Kunai(int, int, int, int);
	Kunai(KunaiStruct);
//	~Kunai();

	//void handleCollision();
	void updateCollisionBox();
	bool move();
	void draw();
};

Kunai::Kunai(int srcX=SCREEN_WIDTH/2, int srcY=SCREEN_HEIGHT/2,
		int dstX=SCREEN_WIDTH/2, int dstY=SCREEN_HEIGHT)
{
	//Load sprite
	sprite = load_image("img/kunai.png");

	x = srcX;
	y = srcY;

	int a = dstX - srcX;
	int b = dstY - srcY;
	double c = sqrt(pow(a,2) + pow(b,2));
	xVel = round(10*a/c);
	yVel = round(10*b/c);

	
	float deg;
	if(xVel != 0)
		deg = yVel<0?-acos(a/c): acos(a/c);
	else
		deg = asin(b/c);

	sprite = sge_transform_surface(sprite, SDL_MapRGBA(sprite->format,0,0,255,0), deg*180/M_PI-90, 1.0, 1.0, 0);
	
	w = sprite->w;
	h = sprite->h;

	collisionBox.x = x+(int)(0.2*w)+0.1*cos(deg)*w;
	collisionBox.y = y+(int)(0.2*h)+0.1*sin(deg)*h;
	collisionBox.w = w-(int)(0.4*w)-0.3*abs(cos(deg))*w;
	collisionBox.h = h-(int)(0.4*h)-0.3*abs(sin(deg))*h;
}

Kunai::Kunai(KunaiStruct ks)
{
	sprite = load_image("img/kunai.png");

	x = ks.x;
	y = ks.y;

	xVel = ks.xVel;
	yVel = ks.yVel;

	float deg; 
	if(xVel!=0) 
		deg = yVel<0?-acos(xVel/10.0): acos(xVel/10.0);
	else
		deg = asin(yVel/10.0);

	sprite = sge_transform_surface(sprite, SDL_MapRGBA(sprite->format,0,0,255,0), deg*180/M_PI-90, 1.0, 1.0, 0);

	w = sprite->w;
	h = sprite->h;

	collisionBox.x = x+(int)(0.2*w)+0.1*cos(deg)*w;
	collisionBox.y = y+(int)(0.2*h)+0.1*sin(deg)*h;
	collisionBox.w = w-(int)(0.4*w)-0.3*abs(cos(deg))*w;
	collisionBox.h = h-(int)(0.4*h)-0.3*abs(sin(deg))*h;
}

void Kunai::updateCollisionBox()
{
	collisionBox.x += xVel;
	collisionBox.y += yVel;
}

bool Kunai::move()
{
	if (x > screen->w+w || x < -w || y > screen->h+h || y < -h) {
		return false;
	}
	x += xVel;
	y += yVel;
	updateCollisionBox();
	return true;
}

void Kunai::draw()
{
//SDL_FillRect(screen, &collisionBox, SDL_MapRGB(screen->format,255,0,0));
	draw_surface(x, y, sprite, screen);
}

class Relic {
public:
	int x,y;
	int xVel, yVel;
	int w,h;

	SDL_Rect collisionBox;

	Relic();
	//virtual ~Relic ();

//	void updateCollisionBox();
	bool move();
	void draw();
private:
	SDL_Surface* sprite;
};

Relic::Relic()
{
	//Load sprite
	sprite = load_image("img/relic.png");

	//Set width and height
	w = sprite->w;
	h = sprite->h;

	x = SCREEN_WIDTH/2 - w/2;
	y = SCREEN_HEIGHT/2 + h/2;

	xVel = 0;
	yVel = 0;

	collisionBox.x = x;
	collisionBox.y = y;
	collisionBox.w = w;
	collisionBox.h = h;
}

bool Relic::move()
{
	x += xVel;
	y += yVel;
//	updateCollisionBox();
	return true;
}

void Relic::draw()
{
	draw_surface(x,y,sprite,screen);
}

//Evil Ninjas trying to kill Kakasi
class EvilNinja {
	private:
	//position and size
	int w,h;

	//sprite
	SDL_Surface* sprite;

	public:
	
	int x,y;
	int xVel, yVel;
	//CollisionBox
	SDL_Rect collisionBox;

	EvilNinja();
	EvilNinja(int, int, int, int);
	~EvilNinja();

	//void handleCollision();
	void updateCollisionBox();
	bool move();
	void draw();
};

EvilNinja::EvilNinja()
{
	//Load sprite
	sprite = load_image("img/ninja.png");

	//Set width and height
	w = sprite->w;
	h = sprite->h;
	
	xVel = 0;
	yVel = 0;

	//Which side of the screen does the ninja come from
	switch(rand()%4) {
		//Top
		case 0:	x = rand()%(screen->w-w);
				y = -h;
				yVel = 8;
				break;
		//Bottom
		case 1: x = rand()%(screen->w-w);
				y = screen->h + h;
				yVel = -8;
				break;
		//Left
		case 2:	x = -w;
				y = rand()%(screen->h-h);
				xVel = 8;
				break;
		//Right
		case 3: x = screen->w + w;
				y = rand()%(screen->h-h);
				xVel = -8;
				break;
		default: ;
	}
	
	collisionBox.x = x+(int)(0.1*w);
	collisionBox.y = y+(int)(0.2*h);
	collisionBox.w = w-(int)(0.1*w);
	collisionBox.h = h-(int)(0.2*h);
}

EvilNinja::EvilNinja(int xPos, int yPos, int xV, int yV)
{
	//Load sprite
	sprite = load_image("img/ninja.png");

	w = sprite->w;
	h = sprite->h;

	x = xPos;
	y = yPos;

	xVel = xV;
	yVel = yV;
	
	collisionBox.x = x+(int)(0.1*w);
	collisionBox.y = y+(int)(0.2*h);
	collisionBox.w = w-(int)(0.1*w);
	collisionBox.h = h-(int)(0.2*h);
}

EvilNinja::~EvilNinja() {} 

void EvilNinja::updateCollisionBox()
{
	collisionBox.x += xVel;
	collisionBox.y += yVel;
}

bool EvilNinja::move()
{
	if (x > screen->w+w || x < -w || y > screen->h+h || y < -h) {
		return false;
	}
	x += xVel;
	y += yVel;
	updateCollisionBox();
	return true;
}

void EvilNinja::draw()
{
	//SDL_Rect rect {x-xVel,y-yVel,w,h};
	draw_surface(x, y, sprite, screen);
	//SDL_FillRect(screen, &collisionBox, SDL_MapRGB(screen->format,255,0,0));
}

//Kakasi aka player class
class Kakasi
{
	private:
		
	//Kunai vector

	public:
	//sprite surface
	SDL_Surface* sprite;

	//kakasi's position
	int x,y;
	int w,h;

	//CollisionBox
	SDL_Rect collisionBox;

	std::vector<Kunai*> kunai;

	//Init variables;
	Kakasi();
	~Kakasi();

	//Return pointer to kunai vector
	std::vector<Kunai*> getKunai();

	void handleInput();
	bool handleEvents();

	//Kakasi throws a kunai
	void throwKunai(Uint16, Uint16);

	void colorize();

	//Draws Kakasi on screen
	void draw();
};

Kakasi::Kakasi()
{
	//Load kakasi sprite and set width and height
	sprite = load_image("img/kakasi.png");
	if (sprite == NULL) {
		fprintf(stderr, "Could not load image %s\n", IMG_GetError());
	}
	w = sprite->w;
	h = sprite->h;
	
	//Set position
	x = (screen->w-w)/2;
	y = (screen->h-h)/2;

	//Make Collision Box
	collisionBox.x = x+(int)(0.2*w);
	collisionBox.y = y+(int)(0.15*h);
	collisionBox.w = w-(int)(0.3*w);
	collisionBox.h = h-(int)(0.45*h);
}

//Kakasi destructor - free sprite surface
Kakasi::~Kakasi() { /*SDL_FreeSurface(sprite);*/ }

std::vector<Kunai*> Kakasi::getKunai() { return kunai; }

void Kakasi::throwKunai(Uint16 dstX, Uint16 dstY)
{
	kunai.push_back(new Kunai(x,y,dstX,dstY));
}

void Kakasi::handleInput()
{
	if(keystate[SDLK_UP]) {y -= 5; collisionBox.y -= 5;} 
	if(keystate[SDLK_DOWN]) {y += 5; collisionBox.y += 5;}
	if(keystate[SDLK_LEFT]) {x -= 5; collisionBox.x -= 5;}
	if(keystate[SDLK_RIGHT]) {x += 5; collisionBox.x += 5;}
	return;
}

bool Kakasi::handleEvents()
{
	if(event.type == SDL_MOUSEBUTTONDOWN) {
		switch(event.button.button) {
			case SDL_BUTTON_LEFT:
				throwKunai(event.button.x, event.button.y); 
				return true;
			default: ;
		}
	}
	return false;
}

void Kakasi::colorize()
{
	sprite->format->Gmask = 0x00002d00;
}

void Kakasi::draw()
{
	//SDL_Rect rect {x-5,y-5,w+10,h+10};
	//SDL_FillRect(screen, &rect, bgcolor);
//	SDL_FillRect(screen, &collisionBox, SDL_MapRGB(screen->format,255,0,0));
	draw_surface(x, y, sprite, screen);
}

//void enemyAction()

bool collisionKunai(EvilNinja* &enemy, std::vector<Kunai*> &kunai)
{
	for(int j=0; j<kunai.size(); j++) {
		if(detectCollision(enemy,kunai[j])) {
			fprintf(stderr,"detect - enemy vs kunai\n");
			delete enemy;
			enemy = NULL;
			delete kunai[j];
			kunai.erase(kunai.begin()+j);
			score += 1;
			break;
		}
	}
}

int main( int argc, char* args[] )
{
	bool running = true;

	if (init_sdl() == false) {
		return 1;
	}

	Kakasi *kakasi, *kakasi2;
	kakasi = new Kakasi();
	Relic * relic = new Relic();

	if (load_data() == false) {
		return 1;
	}
	
	char msgScore[18];
	sprintf(msgScore, "Score: %d", score);
	txtScore = TTF_RenderText_Solid(font, msgScore, textColor);
	if (txtScore == NULL) {
		fprintf(stderr, "Could not render text: %s\n", TTF_GetError());
		return 1;
	}

	txtOver = TTF_RenderText_Shaded(font, " You are DIED! ", {250, 20, 0}, {30,30,30});

	draw_surface(4, 4, txtScore, screen);

	//Mix_PlayMusic(music, -1);

	keystate = SDL_GetKeyState(NULL);
	Uint32 time, dtime;
	int enemyCount = 6;
	std::vector<EvilNinja*> enemy;
	enemy.resize(enemyCount);

	SDL_Surface* txtMenuOne = txtSinglePlayer;
	SDL_Surface* txtMenuTwo = txtMultiPlayer;
	
	char serverAddress[255]="";

	SDL_Surface* txtServerAddress = TTF_RenderText_Solid(menuFont, serverAddress, textColor);
	
	SDL_EnableUNICODE(SDL_ENABLE);

	//Game State variables
	bool menu = true;
	short select = 0;
	bool input = false;
	bool paused = false;
	bool gameover = false;

	bool toFlip = true;

	bool server = false;
	bool client = false;
	while(menu) {
		while(SDL_PollEvent(&event)) {
			toFlip = true;
			if(event.type == SDL_KEYDOWN) {
				if(input) {
					if((event.key.keysym.sym >= SDLK_SPACE &&
					   event.key.keysym.sym <= SDLK_z) ||
					   (event.key.keysym.sym >= SDLK_KP0 &&
					    event.key.keysym.sym <= SDLK_KP9)) {
							char c[2] = {(char)event.key.keysym.unicode,'\0'};
							strcat(serverAddress, c);
							SDL_FreeSurface(txtServerAddress);
							txtServerAddress = TTF_RenderText_Solid(menuFont,
									serverAddress, textColor);
							continue;
					}
					else if(event.key.keysym.sym == SDLK_RETURN)
					{
						join_game(serverAddress);
						input = false;
						menu = false;
						client = true;
						kakasi->colorize();
						continue;
					}
					else if(event.key.keysym.sym == SDLK_BACKSPACE) {
						serverAddress[strlen(serverAddress)-1] = '\0'; 
						SDL_FreeSurface(txtServerAddress);
						txtServerAddress = TTF_RenderText_Solid(menuFont,
									serverAddress, textColor);
						continue;
					}
				}
				switch(event.key.keysym.sym) {
					case SDLK_UP:
					case SDLK_DOWN: select = select==0?1:0; break;
					case SDLK_RETURN: 
						if(netplay) {
							switch(select) {
								case 0:	serve_game();
										menu = false;
										server = true;
										kakasi2->colorize();
										break;
								case 1: input = true;
										break;
								default: ;
							}
						}
						else {
							switch(select) {
								case 0: menu = 0;
										break;
								case 1:	netplay = true;
										txtMenuOne = txtServeGame;
										txtMenuTwo = txtJoinGame;
										init_network();
										kakasi2 = new Kakasi();
								default: ;
							}
						}
					default: ;
				}
			}
			if(event.type == SDL_MOUSEBUTTONUP)
				if(event.button.x > 240 &&
				   event.button.x <= 240+txtSinglePlayer->w &&
				   event.button.y > 510 &&
				   event.button.y <= 510+txtSinglePlayer->h) {
					menu = 0;
					Mix_PlayMusic(music, -1);
				}

			if(event.type == SDL_QUIT) {
				menu = false;
				running = false;
			}
		}

		if(toFlip) {
		SDL_Rect rect = {240, 514+30*select, txtSinglePlayer->w, 30};
		draw_surface(0, 0, menubg, screen);
		SDL_FillRect(screen, &rect , SDL_MapRGBA(screen->format, 255, 0, 0, 125));
		draw_surface(240, 510, txtMenuOne, screen);
		draw_surface(240, 540, txtMenuTwo, screen);
		if(input)
			draw_surface(240+txtMenuTwo->w+4, 539, txtServerAddress, screen);
			
		if(SDL_Flip(screen) != 0) {
			fprintf(stderr, "SDL_Flip error: %s\n", SDL_GetError());
			return 1;
		}
		}
		SDL_Delay(120);
	}

	int data[128];
	int rcvd[128];
	int kunaiCount;
	int enemies;

	if(netplay) {
		if(server) {
			kakasi->x -= kakasi->w/2;
			kakasi2->x += kakasi->w/2;
			kakasi->collisionBox.x -= kakasi->w/2;
			kakasi2->collisionBox.x += kakasi->w/2;
		}
		else {
			kakasi->x += kakasi->w/2;
			kakasi2->x -= kakasi->w/2;
			kakasi->collisionBox.x += kakasi->w/2;
			kakasi2->collisionBox.x -= kakasi->w/2;
		}
	}

	fprintf(stderr,"running");
	Mix_PlayMusic(music, -1);
	//While running
	while(running == true) {
		//Start the frame timer
		time = SDL_GetTicks();
		enemies = 0;
		kunaiCount = 0;
	
		if(netplay) {
		if(SDLNet_CheckSockets(sockSet,0)) {
			if(SDLNet_SocketReady(sock)) {
				if(SDLNet_TCP_Recv(sock,rcvd,512)) {
					if(kakasi2->x != rcvd[0]) {
						kakasi2->collisionBox.x += rcvd[0]-kakasi2->x;
						kakasi2->x = rcvd[0];
					}
					if(kakasi2->y != rcvd[1]) {
						kakasi2->collisionBox.y += rcvd[1]-kakasi2->y;
						kakasi2->y = rcvd[1];
					}
					kunaiCount = rcvd[2];
					int i=0;
					for(i=0; i<kunaiCount;i+=4) {
						KunaiStruct ks = {rcvd[4+i],rcvd[5+i],
							  			  rcvd[6+i],rcvd[7+i]};
						kakasi2->kunai.push_back(new Kunai(ks));
					}
					if(client) {
						enemies = rcvd[3];
						for(int j=0; j<enemies;j+=4) {
						for(int k=0; j<enemyCount; k++) {
							if(!enemy[k]) {
							enemy[k] = new EvilNinja(rcvd[4+j+i], rcvd[5+j+i],
													 rcvd[6+j+i], rcvd[7+j+i]);
							break;
							}
						}
						}
					}
				}
				kunaiCount = 0;
				enemies = 0;
			}
		}
		}
		if(!paused)	kakasi->handleInput();
		
		while(SDL_PollEvent(&event)) {
		
			if(!paused)
				if(kakasi->handleEvents()) {
					kunaiCount+=4;
					fprintf(stderr,"kunai throw\n");
					data[0+kunaiCount] = kakasi->kunai.back()->x;
					data[1+kunaiCount] = kakasi->kunai.back()->y;
					data[2+kunaiCount] = kakasi->kunai.back()->xVel;
					data[3+kunaiCount] = kakasi->kunai.back()->yVel;
				}
			
			if(event.type == SDL_QUIT) 
				running = false;
		}
		
		
		if(!paused) {
			for(int i=0; i<kakasi->kunai.size(); i++) {
				fprintf(stderr,"kunai move\n");
				if(!kakasi->kunai[i]->move()) {
					delete kakasi->kunai[i]; 
					kakasi->kunai.erase(kakasi->kunai.begin()+i);
				}
			}
			if(netplay)
				for(int i=0; i<kakasi2->kunai.size(); i++) {
				fprintf(stderr,"kunai2 move\n");
					if(!kakasi2->kunai[i]->move()) {
						delete kakasi2->kunai[i]; 
						kakasi2->kunai.erase(kakasi2->kunai.begin()+i);
					}
				}
			
			for(int i=0; i<enemyCount; i++) {
				if(!enemy[i]) {
					if(rand()%20 == 0 && !client) {
						enemy[i] = new EvilNinja;
						if(server) {
				fprintf(stderr,"enemy add\n");
							enemies += 4;
							data[0+enemies+kunaiCount]=enemy[i]->x;
							data[1+enemies+kunaiCount]=enemy[i]->y;
							data[2+enemies+kunaiCount]=enemy[i]->xVel;
							data[3+enemies+kunaiCount]=enemy[i]->yVel;
						}
						break;
					}
				    continue;
				}
				if(!enemy[i]->move()) {
					delete enemy[i]; 
					enemy[i] = NULL;
				}
				else {
					if(!Mix_Playing(0))
						Mix_PlayChannel(0, enemySnd, 0);
					if(detectCollision(enemy[i], kakasi)) {
					fprintf(stderr,"detect - enemy vs kakasi\n");
						//kakasi->handleCollision();
					//	paused=true;
					//	gameover=true;
						delete enemy[i];
						enemy[i] = NULL;
						continue;
					}

					if(collisionKunai(enemy[i], kakasi->kunai))
						continue;

					if(netplay) {
						if(detectCollision(enemy[i], kakasi2)) {
							fprintf(stderr,"detect - enemy vs kakasi2\n");
							//kakasi->handleCollision();
							//	paused=true;
							//	gameover=true;
							delete enemy[i];
							enemy[i] = NULL;
							continue;
						}

					fprintf(stderr,"detect - enemy vs kunai2\n");
						collisionKunai(enemy[i], kakasi2->kunai);
					
					}
				}
			}
		}
	
		if(netplay) {
					fprintf(stderr,"prep data\n");
			data[0] = kakasi->x;
			data[1] = kakasi->y;
			data[2] = kunaiCount/4;
			if(server) data[3] = enemies/4;
					fprintf(stderr,"send data\n");
			SDLNet_TCP_Send(sock,data,sizeof(data));
		}

		//Draw objects
		SDL_FillRect(screen, NULL, bgcolor);
		kakasi->draw();
		if(netplay)
			kakasi2->draw();
		for(int i=0; i<enemyCount; i++)
			if(enemy[i]) enemy[i]->draw();
		for (int i = 0; i<kakasi->kunai.size() ; i++) {
			kakasi->kunai[i]->draw();
		}
		if(netplay)
			for (int i = 0; i<kakasi2->kunai.size() ; i++) {
				kakasi2->kunai[i]->draw();
			}

		relic->draw();

		sprintf(msgScore, "Score: %d", score);
		txtScore = TTF_RenderText_Solid(font, msgScore, textColor);
		draw_surface(4, 4, txtScore, screen);

		if(gameover) {
			draw_surface((screen->w-txtOver->w)/2, (screen->h-txtOver->h)/2,
					txtOver, screen);
		}

		if (SDL_Flip(screen) != 0) {
			fprintf(stderr, "SDL_Flip error: %s\n", SDL_GetError());
			return 1;
		}

		dtime = SDL_GetTicks()-time;
		if(dtime < 1000/FPS) {
			SDL_Delay((1000/FPS)-dtime);
		}
	}

	cleanup();
	 
	return 0;    
}


